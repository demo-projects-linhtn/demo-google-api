package com.example.demogoogleapi.service;

import com.example.demogoogleapi.entity.Address;
import com.example.demogoogleapi.entity.Direction;

import java.util.List;

/**
 * Created by linhtn on 9/1/2021.
 */
public interface GeoLocationService {
    List<Address> searchLocation(String fullAddressLine);
    Direction distanceBetween(String origin, String destination);
}
