package com.example.demogoogleapi.service;

import com.example.demogoogleapi.entity.Address;
import com.example.demogoogleapi.entity.Direction;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.PlacesApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.PlacesSearchResponse;
import com.google.maps.model.PlacesSearchResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by linhtn on 9/1/2021.
 */
@Service
public class GeoLocationServiceImpl implements GeoLocationService {

    private final GeoApiContext geoApiContext;

    public GeoLocationServiceImpl(@Value("${gmaps.api.key}") String apiKey) {
        geoApiContext = new GeoApiContext.Builder().apiKey(apiKey)
                .maxRetries(2)
                .connectTimeout(10L, TimeUnit.SECONDS)
                .build();
    }

    @Override
    public List<Address> searchLocation(String fullAddressLine) {
        List<Address> outs = new ArrayList<>();
        try {
            final PlacesSearchResponse placesSearchResponse = PlacesApi.textSearchQuery(geoApiContext, fullAddressLine).await();
            if (placesSearchResponse != null && placesSearchResponse.results.length > 0) {
                for (PlacesSearchResult a : placesSearchResponse.results) {
                    Address address = new Address();
                    address.setFullAddress(a.formattedAddress);
                    address.setLatitude(a.geometry.location.lat);
                    address.setLongitude(a.geometry.location.lng);
                    address.setTypes(a.types);
                    address.setPlaceId(a.placeId);
                    outs.add(address);
                }
            }
        } catch (ApiException | InterruptedException | IOException e) {
            e.printStackTrace();
        }
        return outs;
    }

    @Override
    public Direction distanceBetween(String origin, String destination) {
        try {
            Direction direction = new Direction();
            DirectionsResult directionsResult = DirectionsApi.getDirections(geoApiContext, origin, destination).await();
            direction.setDistance(directionsResult.routes[0].legs[0].distance.inMeters);
            direction.setUnit("meters");
            direction.setOrigin(origin);
            direction.setDestination(destination);
            return direction;
        } catch (ApiException | InterruptedException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
