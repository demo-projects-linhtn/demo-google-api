package com.example.demogoogleapi.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by linhtn on 9/2/2021.
 */
@Getter
@Setter
public class Address {
    private String fullAddress;
    private Double longitude;
    private Double latitude;
    private String[] types;
    private String placeId;
}
