package com.example.demogoogleapi.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by linhtn on 9/6/2021.
 */
@Getter
@Setter
public class Direction {
    Long distance;
    String unit;
    String origin;
    String destination;
}
