package com.example.demogoogleapi.controller;

import com.example.demogoogleapi.entity.Address;
import com.example.demogoogleapi.entity.Direction;
import com.example.demogoogleapi.service.GeoLocationService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by linhtn on 9/1/2021.
 */
@RestController
@RequestMapping("/place")
@AllArgsConstructor
public class GeoLocationController {

    private final GeoLocationService geoLocationService;

    @GetMapping("/search")
    public List<Address> find(@RequestParam String address) {
       return geoLocationService.searchLocation(address);
    }

    @GetMapping("/distance")
    public Direction distanceBetween(@RequestParam String origin, @RequestParam String destination) {
        return geoLocationService.distanceBetween(origin, destination);
    }
}
